package com.muhardin.endy.training.microservice.nasabahservice;

import com.github.javafaker.Faker;
import com.muhardin.endy.training.microservice.nasabahservice.dao.NasabahDao;
import com.muhardin.endy.training.microservice.nasabahservice.entity.Nasabah;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NasabahServiceApplicationTests {

    @Autowired private NasabahDao nasabahDao;
    
    @Test
    public void testGenerateSampleData() {
        Faker faker = new Faker();
        for(int i=0; i<30; i++){
            Nasabah n = new Nasabah();
            n.setNama(faker.name().fullName());
            n.setEmail(faker.name().username() + "@" + faker.internet().domainName());
            n.setNoHp(faker.phoneNumber().cellPhone());
            nasabahDao.save(n);
        }
    }

}
